import json


class BaseFormatter(object):
    start_tag = ''
    end_tag = ''
    separator = ''
    entities = []
    flag = True

    def __init__(self, path):
        self.path = path
        with open(self.path, 'w') as f:
            f.write(self.start_tag)

    def add_record(self, record):
        data = self.make_string_from_entities(record)
        with open(self.path, 'a') as f:
            if self.flag:
                f.write(str(data))
                self.flag = False
            else:
                f.write(self.separator + data)

    def make_string_from_entities(self, record):
        raise NotImplementedError

    def save(self):
        with open(self.path, 'a') as f:
            f.write(self.end_tag)


class JSONFormatter(BaseFormatter):
    start_tag = '['
    end_tag = ']'
    separator = ','

    def make_string_from_entities(self, dict_entity):
        return json.dumps(dict_entity)


class CVSFormatter(BaseFormatter):
    def make_string_from_entities(self, dict_entity):
        pass


def get_formatter(name, *args):
    if name == 'json':
        return JSONFormatter(*args)
    elif name == 'csv':
        return CVSFormatter(*args)

    raise NotImplementedError()

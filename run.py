import argparse
import importlib
import json

from formatters import get_formatter
from importers.base import BaseXMLImporter

def main():
    parser = argparse.ArgumentParser(description='Import property info.')
    parser.add_argument(
        '--source',
        type=str,
        required=True,
    )
    parser.add_argument(
        '--target',
        type=str,
        required=True,
    )
    parser.add_argument(
        '--format',
        type=str,
        required=True,
    )
    parser.add_argument(
        '--limit',
        type=int
    )
    args = parser.parse_args()

    with open('users.json', 'r') as f:
        users = json.loads(f.read())

    importer_instance = BaseXMLImporter(
        users[args.source]['path'],
        get_formatter(args.format, args.target),
        limit=args.limit,
    )
    importer_instance.process()


if __name__ == "__main__":
    main()



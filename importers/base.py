from xml.etree import ElementTree

import requests


class BaseImporter(object):
    path = None
    formatter = None
    initial_data = None
    limit = None

    def __init__(self, path, formatter, limit=None):
        self.path = path
        self.formatter = formatter
        self.limit = limit
        # self.load_initial_data()

    def load_initial_data(self):
        raise NotImplementedError()

    def process(self):
        raise NotImplementedError()


class LocalFileDataLoader(object):
    path = None
    initial_data = None

    def load_initial_data(self):
        with open(self.path, 'r') as f:
            self.initial_data = f.read()


class URLDataLoader(object):
    path = None
    initial_data = None

    def load_initial_data(self):
        self.initial_data = requests.get(self.path).text


class BaseXMLImporter(LocalFileDataLoader, BaseImporter):

    xmlns = '{http://webmaster.yandex.ru/schemas/feed/realty/2010-06}'
    entity_tag = 'offer'

    def process(self):
        entity_number = 0
        for event, elem in ElementTree.iterparse(self.path):
            if event == 'end':
                for entity in elem:
                    if entity.tag != '{}{}'.format(self.xmlns, self.entity_tag):
                        continue
                    dict_entity = {}
                    for field in entity:
                        children = field.getchildren()
                        if children:
                            pass  # Not implemented
                        else:
                            cleaned_tag = field.tag.replace(self.xmlns, '')
                            dict_entity[cleaned_tag] = field.text
                    self.formatter.add_record(dict_entity)
                    entity_number += 1
                    if self.limit and self.limit == entity_number:
                        break
        self.formatter.save()